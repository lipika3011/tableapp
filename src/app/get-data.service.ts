import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map, retry, catchError } from 'rxjs/operators';
import { of } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class GetDataService {

  constructor(private http: HttpClient) { }


  public getData() {
    return this.http.get('../assets/sample-data.json');
  }

  public postData(rowid, rowStatus) {
    let params = new HttpParams();
    params = params.append('id', rowid);
    params = params.append('status', rowStatus);
    return this.http.post('/api/submit', {params})
      .pipe(
        retry(2),
        map(res => JSON.parse(JSON.stringify(res[0]))),
        catchError(err => of(JSON.parse(JSON.stringify(err))))
      );
  }
}
