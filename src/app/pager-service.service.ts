import { Injectable } from '@angular/core';
import * as _ from 'underscore';


@Injectable({
  providedIn: 'root'
})
export class PagerService {

  constructor() { }

  getPager(totalItems: any, currentPage: any = 1, pageSize: any = 10) {
    const totalPages = Math.ceil(totalItems / pageSize);
    let startPage: any;
    let endPage: any;

    if (totalPages <= 5) {
      startPage = 1;
      endPage = totalPages;
    } else {
      if (currentPage <= 3) {
        startPage = 1;
        endPage = 5;
      } else if (currentPage + 1 >= totalPages) {
        startPage = totalPages - 4;
        endPage = totalPages;
      } else {
        startPage = currentPage - 2;
        endPage = currentPage + 2;
      }
    }

    const startIndex = (currentPage - 1) * pageSize;
    const endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

    const pages = _.range(startPage, endPage + 1);

    return {totalItems, currentPage, pageSize, totalPages, startPage, endPage, startIndex, endIndex, pages};
  }

}
