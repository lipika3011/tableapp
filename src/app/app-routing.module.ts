import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TableModule } from './table/table.module';
import { InfiniteTableModule } from './infinite-table/infinite-table.module';

const routes: Routes = [
  {path: 'table',
  loadChildren: './table/table.module#TableModule'},
  {path: 'infiniteTable',
    loadChildren: './infinite-table/infinite-table.module#InfiniteTableModule'},
  { path: '**', redirectTo: 'table' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
