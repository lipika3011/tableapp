import { Component, OnInit } from '@angular/core';
import { GetDataService } from '../../get-data.service';
import { PagerService } from '../../pager-service.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  data: any[] = [];
  pager: any = {};
  pagedItems: any[];

  constructor(private getDataService: GetDataService,
              private pagerService: PagerService) { }

  ngOnInit() {

    this.getDataService.getData()
      .subscribe((value: any) => {
        console.log(value);
        this.data = value;
        this.setPage(1);
      });

  }

  setPage(page: any, rows?: any) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }
    this.pager = this.pagerService.getPager(this.data.length, page, rows);
    this.pagedItems = this.data.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  onChange(event: any) {
    console.log(event);
    this.setPage(1, event);
  }

  onSubmit(rowId: any, rowStatus: any) {
      this.getDataService.postData(rowId, rowStatus)
        .subscribe((val) => {
          console.log(val);
        });
  }

}
