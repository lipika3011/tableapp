import { Component, OnInit } from '@angular/core';
import {GetDataService} from '../../get-data.service';

@Component({
  selector: 'app-infinite-table',
  templateUrl: './infinite-table.component.html',
  styleUrls: ['./infinite-table.component.scss']
})
export class InfiniteTableComponent implements OnInit {
  data: any[] = [];

  constructor(private getDataService: GetDataService) { }

  ngOnInit() {
    this.getDataService.getData()
      .subscribe((value: any) => {
        console.log(value);
        this.data = value;
      });
  }

}
