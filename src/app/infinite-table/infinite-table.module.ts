import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {InfiniteTableComponent} from './infinite-table/infinite-table.component';
import {InfiniteTableRoutingModule} from './infinite-table-routing.module';

@NgModule({
  declarations: [
    InfiniteTableComponent
  ],
  imports: [
    CommonModule,
    InfiniteTableRoutingModule
  ]
})
export class InfiniteTableModule { }
